/*Ryan Marren
 *600.120
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/

#include <stdio.h>
#define MAXLINE 100 
#include <assert.h>
#include <ctype.h> 

int input[MAXLINE]; //will hold input to pass between functions

void getinput(); 
int parsetobytes();

int main()
{
	printf("enter three integers between 0 and 2^32 - 1\n");
	getinput();
	int i;
	int cr, cb, cg;
	cr = cb = cg = 0;
	for (i = 0; i < 3; i++)
	{
		int tmp = parsetobytes();
		unsigned int b = tmp & 0377;
		unsigned int g = (tmp >> 8) & 0377;
		unsigned int r = (tmp >> 16) & 0377;
		switch (i) {
		case 0: cr = r;
			break;
		case 1: cg = g;
			break;
		case 2: cb = b;
			break;
		default:
			break;
		}
		printf("num is %d == rgb(%d, %d, %d) (for testing: == #%08x)\n", tmp, r, g, b, tmp);
	}
	int chex = 0 | cb | (cg << 8) | (cr << 16);
	printf("combo color is rgb(%d, %d, %d) == #%06x == %d \n", cr, cg, cb, chex, chex);
	return 0;
}
//pass user input to global input array for storage
void getinput()
{
	extern int input[MAXLINE];
	int c;
	int i = 0;
	while (((c = getchar()) != EOF) && i < MAXLINE)
	{
		assert(isdigit(c) || c == ' ' || c == '\t' || c == '\n');
		input[i] = c;
		i++;
	} 
	input[i] = '\0';
}
//build first encountered integer from char array, then shift array 
int parsetobytes()
{
	extern int input[MAXLINE];
	int ind, ch, dec;
	dec = ind = 0;
	//skip any preceding whitespace
	while((ch = input[ind]) == '\t' || ch == '\n' || ch == ' ')
		ind++;
	//building a decimial integer from character array, until whitespace
	while((ch = input[ind]) != '\t' &&  ch != '\n' && ch != ' ')
	{
		dec = 10 * dec + (ch - '0');
		ind++;
	}
	ind++;
	int nind = 0;
	//deleting the previous byte and shifting the array over
	while(input[ind] != '\0')
	{
		input[nind] = input[ind];
		ind++;
		nind++;
	}
	input[nind] = input[ind];
	return dec;
}
