/*Ryan Marren
 *600.120
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/

#include <stdio.h>
#define MAXLINE 1000 
#include <assert.h>
#include <ctype.h> 

int input[MAXLINE]; //will hold input to pass between functions

void getinput(); 
int parsetobytes();
void tableinit();
void addrow(int c1, int c2, int c3, int chex);
void tableend();

int main()
{
	printf("enter sets of three integers between 0 and 2^32 - 1\n");
	tableinit();
	getinput();
	int i;
	while(input[0] != '\0')
	{
		unsigned int cr, cb, cg;
		unsigned int colors[3];
		cr = cb = cg = 0;
		for (i = 0; i < 3; i++)
		{
			unsigned int tmp = parsetobytes();
			colors[i] = tmp;
			unsigned int b = tmp & 0377;
			unsigned int g = (tmp >> 8) & 0377;
			unsigned int r = (tmp >> 16) & 0377;
			switch (i) {
			case 0: cr = r;
				break;
			case 1: cg = g;
				break;
			case 2: cb = b;
				break;
			default:
				break;
			}	
		}		

		int chex = 0 | cb | (cg << 8) | (cr << 16);
		addrow(colors[0], colors[1], colors[2], chex);
	}
	tableend();
	return 0;
}
//pass user input to global input array for storage
void getinput()
{
	
	extern int input[MAXLINE];
	int c;
	int i = 0;
	while (((c = getchar()) != EOF) && i < MAXLINE)
	{
		assert(isdigit(c) || c == ' ' || c == '\n' || c == '\t');
		input[i] = c;
		i++;
	} 
	input[i] = '\0';
}
//build first encountered integer from char array, then shift array 
int parsetobytes()
{
	extern int input[MAXLINE];
	int ind, ch, dec;
	dec = ind = 0;
	//clear predecing whitespace
	while((ch = input[ind]) == '\t' ||  ch == '\n' || ch == ' ')
		ind++;
	//building a decimial integer from character array, until whitespace
	while((ch = input[ind]) != '\t' &&  ch != '\n' && ch != ' ' && ch != EOF)
	{
		dec = 10 * dec + (ch - '0');
		ind++;
	}
	if(ch != EOF)
		ind++;
	int nind = 0;
	//deleting the previous byte and shifting the array over
	while(input[ind] != '\0')
	{
		input[nind] = input[ind];
		ind++;
		nind++;
	}
	input[nind] = input[ind];
	return dec;
}

void tableinit()
{
	printf("<html>\n<head>\n<title>Color Combination Table</title>\n</head>\n<body text = \"FFFFFF\">\n<table>\n");
}

void addrow(int c1, int c2, int c3, int chex)
{
	printf("<tr>\n"
		"<td bgcolor=\"#%06x\"><font color=\"#%06x\">#%06x\n</font>"	
		"<td bgcolor=\"#%06x\"><font color=\"#%06x\">#%06x\n</font>"
		"<td bgcolor=\"#%06x\"><font color=\"#%06x\">#%06x\n</font>"
		"<td bgcolor=\"#%06x\"><font color=\"#%06x\">#%06x\n</font>"
		"</tr>\n"
		, c1, 0xFFFFFF - c1, c1, c2, 0xFFFFFF - c2, c2, c3, 0xFFFFFF - c3, c3, chex, 0xFFFFFF - chex, chex);
}

void tableend()
{
	printf("</table>\n</body>\n</html>");
}
