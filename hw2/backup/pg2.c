#define WORDSIZE 31
#define BUFFERSIZE 300000 
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

void load_dict(char[], char[][WORDSIZE]);

bool is_valid_entry(char[]);

void load_input(char[], char[][WORDSIZE]);

void normalize(char[], char[]);

int linear_search(char[][WORDSIZE], char[]);

int main(int argc, char *argv[])
{
	//Ensure that 2 args are present.
	if(argc != 3)
	{
		printf("There has to be 3 arguments for the program to work!!!");
		return 1;
	}
	static char dict[BUFFERSIZE][WORDSIZE];
	static char input[BUFFERSIZE][WORDSIZE];
	load_dict(argv[1], dict);
	load_dict(argv[2], input);
	int i = 0;
	while(input[i][0] != '\0')
	{
		if(is_valid_entry(input[i]))
		{
			char dummy[WORDSIZE];
			normalize(input[i], dummy);
			if(linear_search(dict, dummy) == -1)
				printf("%s\n", dummy);
		}
		i++;
	}	
	return 0;
}
//called load_dict, but is re-used to load the input (as it is the same process)
void load_dict(char filename[], char dict[][WORDSIZE])
{
	//read file
	FILE *dictf;
	if((dictf = fopen( filename, "r")) == 0)
	{
		fprintf(stderr, "Program can't open %s\n", filename); 
		exit(1);
	}
	char ch = 0;
	int entry, letter;
	entry = letter = 0;
	//split by whitespace
	while((ch = getc(dictf)) != EOF)
	{
		// \t and ' ' added to make compatable with input array
		if(ch == '\n' || ch == '\t' || ch == ' ')
		{
			dict[entry++][letter] = '\0';
			letter = 0;
		}
		else
		{
			dict[entry][letter++] = ch;
		}
	
	}
	dict[entry][letter] = '\0'; // while loop will not end last entry
}

bool is_valid_entry(char input[])
{
	//check if first char is letter or ' or "
	if(input[0] != '\"' && input[0] != '\'' && !isalpha(input[0]))
	{
		return false;
	}
	int i = 1;
	//check if all middle chars are letters or '-'
	while(input[i+1] != '\0')
	{
		if(input[i] != '-' && !isalpha(input[i]))
			return false;
		i++;	
	}
	//check is last char is . , ; : ' " ? or ! in a horrendously long line
	if(input[i] == '.' || input[i] == ',' || input[i] == ';' || input[i] == ':' || input[i] == '\'' || input[i] == '\"' || input[i] == '?' || input[i] == '!' || isalpha(input[i]))
	{
		return true;
	}
	return false;
}
void normalize(char input[], char output[])
{
	int i = 0;
	int k = 0;
	while(input[i] != '\0')
	{
		if(isalpha(input[i]) || input[i] == '-')
			output[k++] = tolower( input[i] );
		i++;
	}
	output[k] = '\0';
}
int linear_search(char dict[][WORDSIZE], char word[])
{
	int i = 0;
	while(dict[i][0] != '\0')
	{
		if(strcmp(word, dict[i]) == 0)
			return i;
		i++;
	}
	return -1;
}
