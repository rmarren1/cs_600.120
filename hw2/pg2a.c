/*Ryan Marren
 *600.120
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#include <stdio.h>
#include "spellcheck.h"
#include <stdlib.h>

int main(int argc, char *argv[])
{
	//Ensure that 2 args are present.
	if(argc != 3)
	{
		fprintf(stderr, "There has to be 3 arguments for the program to work!!!\n");
		exit(1);
	}
	static char dict[BUFFERSIZE][WORDSIZE];
	static char input[BUFFERSIZE][WORDSIZE];
	load_dict(argv[1], dict);
	load_dict(argv[2], input);
	int dictsize = 0;
	while(dict[dictsize][0] != '\0')
		dictsize++;
	int i = 0;
	while(input[i][0] != '\0')
	{
		if(is_valid_entry(input[i]))
		{
			char dummy[WORDSIZE];	
			normalize(input[i], dummy);
			if(linear_search(dict, dummy) == -1)
				printf("%s\n", dummy);
		}
		i++;
	}	
	return 0;
}

