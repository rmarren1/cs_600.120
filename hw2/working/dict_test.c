/*Ryan Marren
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#include <stdio.h>
#include <stdbool.h>
#include "spellcheck.h"
#include <stdlib.h>
#include <assert.h>

int main(int argc, char *argv[])
{
	//Ensure that 2 args are present.
	if(argc != 3)
	{
		fprintf(stderr, "There has to be 3 arguments for the program to work!!!\n");
		exit(1);
	}
	static char dict[BUFFERSIZE][WORDSIZE];
	static char input[BUFFERSIZE][WORDSIZE];
	load_dict(argv[1], dict);
	load_dict(argv[2], input);
	//load_dict test
	printf("***load_dict test***\n");
	int dictsize = 0;
	while(dict[dictsize][0] != '\0')
		dictsize++;
	printf("The size of the dictionary array is: %d\n", dictsize);
	int inputsize = 0;
	while(input[inputsize][0] != '\0')
		inputsize++;
	printf("The size of the input array is: %d\n\n", inputsize);
	
	//is_valid_entry test 
	printf("***is_valid_entry test***\n");
	bool valid;
	valid = is_valid_entry("word");
	assert(valid);
	valid = is_valid_entry("\"word\"");
	assert(valid);
	valid = is_valid_entry("\"wo-rd\"");
	assert(!valid);
	valid = is_valid_entry("\"\"word\"\"");
	assert(!valid);
	valid = is_valid_entry("word??");
	assert(!valid);
	valid = is_valid_entry("w0rd");
	assert(!valid);
	
	printf("All is_valid_entry() tests passed.\n\n");

	//normalize test
	printf("***normalize test***\n");
	char normalized[WORDSIZE];
	printf("Original:\t%s\n", "Hello!");
	normalize("Hello!", normalized), printf("Normalized:\t%s\n", normalized);
	printf("Original:\t%s\n", "HELLO");
	normalize("HELLO", normalized), printf("Normalized:\t%s\n", normalized);
	printf("Original:\t%s\n", "\"hello\"");
	normalize("\"hello\"", normalized), printf("Normalized:\t%s\n", normalized);
	printf("Original:\t%s\n", "\'hello?\'");
	normalize("h-e-l-l-o", normalized), printf("Normalized:\t%s\n", normalized);
	printf("Original:\t%s\n", "HeLlO!");
	normalize("HeLlO!", normalized), printf("Normalized:\t%s\n", normalized );
	printf("Original:\t%s\n", "hello");
	normalize("hello", normalized), printf("Normalized:\t%s\n", normalized);
	
	//linear_search test
	printf("\n***linear_search test***\n");
	if(linear_search(dict, "spell") != -1)
		printf("\"spell\" is in the dictionary.\n");
	if(linear_search(dict, "bleuag") == -1)
		printf("\"bleuag\" is not in the dictionary.\n");

	//binary_search test
	printf("\n***binary_search test***\n");
	char dummy[WORDSIZE];
	if(binary_search(dummy, dict, "spfdsal", 0, dictsize) == -1)
		printf("\"spfdsal\" is not in the dictionary, the last word checked was %s\n", dummy);
	if(binary_search(dummy, dict, "bleuag", 0, dictsize) == -1)
		printf("\"bleuag\" is not in the dictionary, the last string checked was %s\n", dummy);
	return 0;
}

