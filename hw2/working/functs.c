/*Ryan Marren
 *600.120
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#include "spellcheck.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

//called load_dict, but is re-used to load the input (as it is the same process)
void load_dict(char filename[], char dict[][WORDSIZE])
{
	//read file
	FILE *dictf;
	if((dictf = fopen( filename, "r")) == 0)
	{
		fprintf(stderr, "Program can't open %s\n", filename); 
		exit(1);
	}
	char ch = 0;
	int entry, letter;
	entry = letter = 0;
	//split by whitespace
	while(((ch = getc(dictf)) != EOF) && letter < WORDSIZE)
	{
		// \t and ' ' added to make compatable with input array
		if(ch == '\n' || ch == '\t' || ch == ' ')
		{
			dict[entry++][letter] = '\0';
			letter = 0;
			if(entry >= BUFFERSIZE)
				return;
		}
		else
		{
			dict[entry][letter++] = ch;
		}
	
	}
	dict[entry][letter] = '\0'; // while loop will not end last entry
}

bool is_valid_entry(char input[])
{
	//check if first char is letter or ' or "
	if(input[0] != '\"' && input[0] != '\'' && !isalpha(input[0]))
	{
		return false;
	}
	int i = 1;
	//check if all middle chars are letters or '-'
	while(input[i+1] != '\0')
	{
		if(!isalpha(input[i]))
			return false;
		i++;	
	}
	//check is last char is . , ; : ' " ? or ! in a horrendously long line
	if(input[i] == '.' || input[i] == ',' || input[i] == ';' || input[i] == ':' || input[i] == '\'' || input[i] == '\"' || input[i] == '?' || input[i] == '!' || isalpha(input[i]))
	{
		return true;
	}
	return false;
}
void normalize(char input[], char output[])
{
	int i = 0;
	int k = 0;
	while(input[i] != '\0')
	{
		if(isalpha(input[i]))
			output[k++] = tolower( input[i] );
		i++;
	}
	output[k] = '\0';
}
int linear_search(char dict[][WORDSIZE], char word[])
{
	int i = 0;
	while(dict[i][0] != '\0')
	{
		if(strcmp(word, dict[i]) == 0)
			return i;
		i++;
	}
	return -1;
}
int binary_search(char check[], char dict[][WORDSIZE], char word[], int start, int end)
{
	if(start > end)
		return -1;
	int middle = (start + end) / 2;
	strcpy(check, dict[middle-1]);

	if(strcmp(word, check) > 0)
		return binary_search(check, dict, word, middle + 1, end);
	if(strcmp(word, check) < 0)
		return binary_search(check, dict, word, start, middle-1);
	return middle;
}
