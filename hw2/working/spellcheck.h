/*Ryan Marren
 *600.120
 *9/7/14
 *Assignment 1
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#ifndef SPELLCHECK_H
#define SPELLCHECK_H
#include <stdbool.h>
#define WORDSIZE 31 
#define BUFFERSIZE 300000

void load_dict(char[], char[][WORDSIZE]);

bool is_valid_entry(char[]);

void load_input(char[], char[][WORDSIZE]);

void normalize(char[], char[]);

int linear_search(char[][WORDSIZE], char[]);

int binary_search(char[], char[][WORDSIZE], char[], int, int);
#endif
