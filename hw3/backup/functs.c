/*Ryan Marren
 *600.120
 *9/27/14
 *Assignment 3 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#include "functs.h"
void swag(Array2 *a)
{
	initialize2(a, INITIALSIZE);
	add2(a);
	add(a->array[0], 'D');
	add(a->array[0], 't');
	add(a->array[0], '\0');
	add2(a);
	add(a->array[1], 'e');
	add(a->array[1], '\0');
	add2(a);
	add(a->array[2], 'e');
	add(a->array[2], '\0');
	add2(a);
	add(a->array[3], 'e');
	add(a->array[3], '\0');
}

void get_input(char filename[], Array2 *words)
{
	initialize2(words, INITIALSIZE);
	char ch = 0;
	FILE *dictf;
	//read file
	if((dictf = fopen( filename, "r")) == 0)
	{
		fprintf(stderr, "Program can't open %s\n", filename); 
		exit(1);
	}
	//split by whitespace
	add2(words);
	while((ch = getc(dictf)) != EOF)
	{
		if(ch == '\n' || ch == '\t' || ch == ' ')
		{
			add(words->array[(words->used)-1], '\0');
			add2(words);
		}
		else
		{
			add(words->array[(words->used)-1], ch);
		}
	}
	add(words->array[(words->used)-1], '\0');
}
int initialize(Array *a, int init_size)
{
	a->array = malloc(sizeof(char)*init_size);
	a->used = 0;
	a->size = init_size * sizeof(char);
	a->valid = false;
	return 0;
}

void add(Array *a, char element)
{
	if(a->used == a->size)
	{
		a->size *= 2;
		char *tmp = NULL;
		tmp = realloc(a->array, a->size);
		a->array = tmp;
		if(a->array == NULL)
		{
			printf("There was an error reallocating!");
			return;
		}
		if(tmp == NULL)
			free(tmp);
		tmp = NULL;
	}
	a->array[a->used++] = element;
}

void freeArray(Array *a)
{
	free(a->array);
	a->used = 0;
	a->size = 0;
}

void printarray(Array *a)
{
	printf("%s\t%d\n", a->array, a->valid);
}
int initialize2(Array2 *a, int init_size)
{
	a->array = malloc(sizeof(void*)*init_size);
	a->used = 0;
	a->size = init_size * sizeof(void*);
	return 0;
}

void add2(Array2 *a)
{
	if(a->used == a->size)
	{
		a->size *= 2;
		Array **tmp = NULL;
		tmp = realloc(a->array, a->size);
		a->array = tmp;
		if(a->array == NULL)
		{
			printf("There was an error reallocating!");
			return;
		}
		if(tmp == NULL)
		{
			free(tmp);
		}
		tmp = NULL;
	}
	a->array[a->used++] = malloc(sizeof(void*));
	initialize(a->array[a->used-1], INITIALSIZE);
}

void freeArray2(Array2 *a)
{
	free(a->array);
	a->used = 0;
	a->size = 0;
}

bool is_valid_entry(char input[])
{
	//check if first char is letter or ' or "
	if(input[0] != '\"' && input[0] != '\'' && !isalpha(input[0]))
	{
		return false;
	}
	int i = 1;
	//check if all middle chars are letters or '-'
	while(input[i+1] != '\0')
	{
		if(!isalpha(input[i]))
			return false;
		i++;	
	}
	//check is last char is . , ; : ' " ? or ! in a horrendously long line
	if(input[i] == '.' || input[i] == ',' || input[i] == ';' || input[i] == ':' || input[i] == '\'' || input[i] == '\"' || input[i] == '?' || input[i] == '!' || isalpha(input[i]))
	{
		return true;
	}
	return false;
}
void normalize(char input[], char output[])
{
	int i = 0;
	int k = 0;
	while(input[i] != '\0')
	{
		if(isalpha(input[i]))
			output[k++] = tolower( input[i] );
		i++;
	}
	output[k] = '\0';
}
void process_input(Array2 *words, Array2 *normalized)
{
	bubble_sort(words);
	size_t i = 0;
	initialize2(normalized, words->size);
	while(i < words->used)
	{
		words->array[i]->valid = is_valid_entry(words->array[i]->array);
		add2(normalized);
		normalize(words->array[i]->array, normalized->array[i]->array);
		i++;
	}
}
void bubble_sort (Array2 *a) {
	int n = a->used;
        int i = 1;
	bool s = true;
        while (s) {
            s = false;
            for (i = 1; i < n; i++) {
                if (strcmp(a->array[i]->array, a->array[i-1]->array) < 0) {
		    swap(a->array[i], a->array[i-1]);
                    s = true;
            }
        }
    }
}
void swap(Array *p1, Array *p2)
{
    Array temp = *p1;
    temp = *p1;
    *p1 = *p2;
    *p2 = temp;
    p1 = &temp;
}
