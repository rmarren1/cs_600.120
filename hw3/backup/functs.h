/*Ryan Marren
 *600.120
 *9/27/14
 *Assignment 3 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu*/
#ifndef FUNCTS_H
#define FUNCTS_H
#define INITIALSIZE 1000 
#include "functs.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>

typedef struct{
	char *array;
	size_t used;
	size_t size;
	bool valid;
} Array;

typedef struct{
	Array **array;
	size_t used;
	size_t size;
} Array2;

void swap(Array *, Array *);

void bubble_sort(Array2 *);

void printarray(Array *a);

bool is_valid_entry(char []);

void process_input(Array2 *a, Array2 *b);

int initialize(Array *a, int init_size);

void add(Array *a, char element);

void freeArray(Array *a);

int initialize2(Array2 *a, int init_size);

void add2(Array2 *a);

void freeArray2(Array2 *a);

void get_input(char [], Array2*);
#endif
