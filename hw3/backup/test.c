#include "functs.h"
#include <stdio.h>
#include <stdlib.h>

void swag(Array2 *a);

int main(int argc, char *argv[])
{
	//Make sure user has correct number of arugments.
	if(argc != 3)
	{
		fprintf(stderr, "You didn't put the correct number of arguments! There should be one input file and one output file.\n");
		return 1;
	}	
	/*Array *x;
	Array *y;
	x = malloc(sizeof(void*));
	y = malloc(sizeof(void*));
	initialize(x, 4);
	initialize(y, 4);
	add(x, 'x');
	add(y, 'y');
	swap(x, y);
	printarray(x);
	printarray(y);*/
	Array2 *w;
	Array2 *n;
	n = malloc(sizeof(void*));
	w = malloc(sizeof(void*));
	get_input(argv[1], w);
	process_input(w, n);
	size_t i = 0;
	while(i < w->used)
	{
		printarray(w->array[i]);
		printarray(n->array[i++]);
	}	
}

