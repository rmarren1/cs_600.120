/*Ryan Marren and Jenny Wagner
 *600.120
 *10/1/14
 *Assignment 3 
 *845-548-4791//443-883-0936
 *rmarren1//jwagne47
 *rmarren1@jhu.edu//jwagner@jhu.edu
*/
#include "functs.h"
void swag(Array2 *a)
{
	initialize2(a, INITIALSIZE);
	add2(a);
	add(a->array[0], 'D');
	add(a->array[0], 't');
	add(a->array[0], '\0');
	add2(a);
	add(a->array[1], 'e');
	add(a->array[1], '\0');
	add2(a);
	add(a->array[2], 'e');
	add(a->array[2], '\0');
	add2(a);
	add(a->array[3], 'e');
	add(a->array[3], '\0');
}

void get_input(char filename[], Array2 *words)
{
	initialize2(words, INITIALSIZE);
	char ch = 0;
	FILE *dictf;
	//read file
	if((dictf = fopen( filename, "r")) == 0)
	{
		fprintf(stderr, "Program can't open %s\n", filename); 
		exit(1);
	}
	//split by whitespace
	add2(words);
	while((ch = getc(dictf)) != EOF)
	{
		if(ch == '\n' || ch == '\t' || ch == ' ')
		{
			add(words->array[(words->used)-1], '\0');
			add2(words);
		}
		else
		{
			add(words->array[(words->used)-1], ch);
		}
	}
	add(words->array[(words->used)-1], '\0');
	fclose(dictf);
}
int initialize(Array *a, int init_size)
{
	a->array = malloc(sizeof(char)*init_size);
	a->used = 0;
	a->size = init_size * sizeof(char);
	a->valid = false;
	a->freq = 0;
	return 0;
}

void add(Array *a, char element)
{
	if(a->used == a->size)
	{
		a->size *= 2;
		a->array = realloc(a->array, a->size);
		if(a->array == NULL)
		{
			printf("There was an error reallocating!");
			return;
		}
	}
	a->array[a->used++] = element;
}

void freeArray(Array *a)
{
	free(a->array);
	a->used = 0;
	a->size = 0;
	free(a);
}

void printarray(Array2 *a)
{
	size_t i;
	printf("Welcome to the Simply Amazing Ocurence Counter (SOAC)!\n\nWord frequencies from your input are:\n");
	for(i = 0; i < a->used; i++)
	{
		if(a->array[i]->valid)
		{
			printf("%-10s", a->array[i]->array);
			int k;
			for(k = 0; k < a->array[i]->freq; k++)
				printf("*");
			printf("\n");
			i += (a->array[i]->freq - 1);
		}
	}
	printf("Enjoy your delicious [annotated] source, which is now in file output.txt,\nand please come back soon for another serving!\n");
}
void makeoutput(Array2 *a)
{	
	FILE *fp;
	fp = fopen("output.txt", "w");
	size_t i;
	for(i = 0; i < a->used; i++)
	{
		if(a->array[i]->freq > 1)
		{
			fprintf(fp, "%s[%d] ", a->array[i]->array, a->array[i]->freq);
		}
		else
		{
			
			fprintf(fp, "%s ", a->array[i]->array);
		}	
	}	
	fclose(fp);	
}
int initialize2(Array2 *a, int init_size)
{
	a->array = malloc(sizeof(Array)*init_size);
	a->used = 0;
	a->size = init_size * sizeof(Array);
	return 0;
}

void add2(Array2 *a)
{
	if(a->used == (a->size/sizeof(Array)))
	{
		a->size *= 2;
		a->array = realloc(a->array, a->size);
		if(a->array == NULL)
		{
			printf("There was an error reallocating!");
			return;
		}
	}
	a->array[a->used] = malloc(sizeof(Array));
	initialize(a->array[a->used], INITIALSIZE);
	a->used++;
}

void freeArray2(Array2 *a)
{
	size_t i;
	i = 0;
	for(; i < a->used; i++)
	{
		freeArray(a->array[i]);
	}
	free(a->array);
	a->used = 0;
	a->size = 0;
	free(a);
}

bool is_valid_entry(char input[])
{
	//check if first char is letter or ' or "
	if(input[0] != '\"' && input[0] != '\'' && !isalpha(input[0]))
	{
		return false;
	}
	int i = 1;
	//check if all middle chars are letters or '-'
	while(input[i+1] != '\0')
	{
		if(!isalpha(input[i]))
			return false;
		i++;	
	}
	//check is last char is . , ; : ' " ? or ! in a horrendously long line
	if(input[i] == '.' || input[i] == ',' || input[i] == ';' || input[i] == ':' || input[i] == '\'' || input[i] == '\"' || input[i] == '?' || input[i] == '!' || isalpha(input[i]))
	{
		return true;
	}
	return false;
}
void normalize(char input[], char output[])
{
	int i = 0;
	int k = 0;
	while(input[i] != '\0')
	{
		if(isalpha(input[i]))
			output[k++] = tolower( input[i] );
		i++;
	}
	output[k] = '\0';
}
void process_input(Array2 *words, Array2 *normalized)
{
	size_t i = 0;
	initialize2(normalized, words->size);
	while(i < words->used)
	{
		words->array[i]->valid = is_valid_entry(words->array[i]->array);
		
		add2(normalized);
		normalize(words->array[i]->array, normalized->array[i]->array);
		normalized->array[i]->valid = words->array[i]->valid;
		i++;
	}
	for(i = 0; i < normalized->used; i++)
	{
		size_t k;
		for(k = 0; k < normalized->used; k++)
		{
			if(strcmp(normalized->array[i]->array, normalized->array[k]->array) == 0 && strlen(normalized->array[i]->array) && strlen(normalized->array[k]->array)){normalized->array[i]->freq += 1;
			words->array[i]->freq += 1;}	
		}
	}
	bubble_sort(normalized);
}
void bubble_sort (Array2 *a) {
	int n = a->used;
        int i = 1;
	bool s = true;
        while (s) {
            s = false;
            for (i = 1; i < n; i++) {
                if (strcmp(a->array[i]->array, a->array[i-1]->array) < 0) {
		    swap(a->array[i], a->array[i-1]);
                    s = true;
            }
        }
    }
}
void swap(Array *p1, Array *p2)
{
        Array temp;
        temp = *p1;
        *p1 = *p2;
        *p2 = temp;
        p1 = &temp;
}
