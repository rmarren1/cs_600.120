/*Ryan Marren and Jenny Wagner
 **600.120
 *10/1/14
 *Assignment 3 
 *845-548-4791//443-883-0936
 *rmarren1//jwagne47
 *rmarren1@jhu.edu//jwagner@jhu.edu
*/
#include "functs.h"
#include <stdio.h>
#include <stdlib.h>

void swag(Array2 *a);

int main(int argc, char *argv[])
{
	//Make sure user has correct number of arugments.
	if(argc != 2)
	{
		fprintf(stderr, "You didn't put the correct number of arguments! There should be one input file.\n");
		return 1;
	}	
	/*Array2 *x;
	x = malloc(sizeof(Array2));
	initialize2(x, 4);
	for(int i = 0; i < 100; i++)
		add2(x);
	freeArray2(x);*/
	Array2 *w;
	Array2 *n;
	n = malloc(sizeof(Array2));
	w = malloc(sizeof(Array2));
	get_input(argv[1], w);
	process_input(w, n);
	printarray(n);
	makeoutput(w);
	freeArray2(n);
	freeArray2(w);
}

