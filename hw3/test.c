/*Ryan Marren and Jenny Wagner
 *600.120
 *10/1/14
 *Assignment 3 
 *845-548-4791//443-883-0936
 *rmarren1//jwagne47
 *rmarren1@jhu.edu//jwagner@jhu.edu
*/
#include "functs.h"
#include <stdio.h>
#include <stdlib.h>

void swag(Array2 *a);

int main()
{
	//test for Array2 struct, and initialize2, add2, and freeArray2 functions
	Array2 *x;
	x = malloc(sizeof(Array2));
	initialize2(x, 4);
	for(int i = 0; i < 1000; i++)
		add2(x);
	freeArray2(x);
	printf("Array2 successfully added 1000 Array elements and then freed them all\n");
	//test for Array struct, and initialize, add, and freeArray functions
	Array *y;
	y = malloc(sizeof(Array));
	initialize(y, 4);
	for(int i = 0; i < 1000; i++)
		add(y, 'y');
	freeArray(y);
	printf("Array successfully added 1000 char elements and then freed them all\n");
	Array2 *t;
	t = malloc(sizeof(Array2));
	initialize2(t, 4);
	add2(x);
	add2(x);
	add2(x);
	add(t->array[0], 'b');
	add(t->array[0], '\0');
	add(t->array[1], 'a');
	add(t->array[1], '\0');
	add(t->array[2], 'c');
	add(t->array[2], '\0');
	printf("Order is: %s %s %s\n", t->array[0]->array, t->array[1]->array, t->array[2]->array);
	bubble_sort(t);
	printf("Order is: %s %s %s\n", t->array[0]->array, t->array[1]->array, t->array[2]->array);
	printf("bubble_sort test complete.\n");
}

