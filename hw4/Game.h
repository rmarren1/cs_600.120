/** Game of cards for homework 4.  (Try to put things here instead of main.
 */

#ifndef CARDGAME_H
#define CARDGAME_H

#include "Player.h"
#include <stdbool.h>

typedef struct {

} Game;

/** Initialize the game, getting using input for the player's names.
 */
void initPlayers(Game *);

/** Initialie a full deck of cards for the game to use.
 */
void createDeck(Game *);

/** Shuffle and deal the deck round-robin to all the players.
 */
void dealRound(Game *);

/** Print the names and points of all the players.
 */
void printPoints(Game *);

/** Play a round (hand) of the game, from dealing through to printing points.
 */
void playRound(Game *); 

/** Play only one trick of the game.
 */
void playTrick(Game *);


#endif
