#include "Card.h"
#include <stdio.h>
/** Display the name of the player and the cards in the hand of a
    player, lettering the cards starting with 'a' as part of the
    display.
 */
void printHand(const Player *p)
{	
	const char* suitNames[] = {"Circle, Square, Triangle, Diamond"};
	Card *curr = p->head;
	char i = 'a';
	printf("%s\n", p->name);
	while(curr->next != null)
	{
		printf("%c) %s %d\n", i++, suitNames[curr->suit], curr->val);
	}
}

/** Play a card selected by letter (comes out of hand).
    Return pointer to the Card played.
 */
Card * play(Player *, char letter);

/** Add a card to a Player's hand.
 */
void addCard(Player *p, Card *c)
{
	c->next = p->head;
	p->head = c;
}

/** This player takes the trick just played by all.
 */
void takeTrick(Player *, CNode * trick);
