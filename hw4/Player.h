/* Player class for a player in a (Shapes) card game
 */

#ifndef PLAYER_H
#define PLAYER_H

#include "Card.h"

typedef struct {
	Card *head;
	char name[];
} Player;

/** Display the name of the player and the cards in the hand of a
    player, lettering the cards starting with 'a' as part of the
    display.
 */
void printHand(const Player *);

/** Play a card selected by letter (comes out of hand).
    Return pointer to the Card played.
 */
Card * play(Player *, char letter);

/** Add a card to a Player's hand.
 */
void addCard(Player *, Card *);

/** This player takes the trick just played by all.
 */
void takeTrick(Player *, CNode * trick);

#endif
