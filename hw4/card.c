/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#ifndef CARD_H
#define CARD_H
#include "card.h"
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#define SIZEOFDECK 60
#define DECK 4

typedef enum {
	SENTINEL, SQUARE, TRIANGLE, DIAMOND, CIRCLE
} suit_v;

typedef struct {
	suit_v suit;
	int val; 	
} Card;

typedef struct CNode{
	Card *card;
	struct CNode *next;
} CNode;

void swap(CNode *i, CNode *j)
{
	Card tmp;
	tmp = *(i->card);
	*(i->card) = *(j->card);
	*(j->card) = tmp;
}

CNode* cardAtIndex(CNode *c, int index)
{
	while(c->card->val && index)
	{
		return cardAtIndex(c->next, index - 1);
	}	
	return c;
}

void killList(CNode **lptr)
{
	if((*lptr) == NULL)
		return;
	if((*lptr)->card->val)
		killList(&(*lptr)->next);
	free((*lptr)->card);
	(*lptr)->card = NULL;
	free(*lptr);
	*lptr = NULL;
} 		

#endif 
