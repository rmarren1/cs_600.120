/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#ifndef CARD_H
#define CARD_H
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#define SIZEOFDECK 60
#define DECK 4

typedef enum {
	SENTINEL, SQUARE, TRIANGLE, DIAMOND, CIRCLE
} suit_v;

typedef struct {
	suit_v suit;
	int val; 	
} Card;

typedef struct CNode{
	Card *card;
	struct CNode *next;
} CNode;

void swap(CNode *i, CNode *j);

CNode* cardAtIndex(CNode *c, int index);

void killList(CNode **lptr);

#endif 
