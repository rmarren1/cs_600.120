/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#include "player.h"

const char* SuitNames[] = {"sentinel\0", "(Sq) ■ \0", "(Tr) ▲\0", "(Di) ◆\0", "(Ci) ●\0"};

typedef struct {
	Player **players;
	Card highest;
	Player **winning;
	bool circles;
	suit_v suit;
} Game;

/** Initialize the game, getting using input for the player's names.
 */
void initPlayers(Game *g)
{
	g->players = malloc(5 * sizeof(void*));
	int i;
	for (i = 0; i < 4; i++)
	{
		g->players[i] = malloc(sizeof(Player));
		g->players[i]->hand = NULL;
		g->players[i]->lead = false;
		g->players[i]->count = 0;
		g->players[i]->points = 0;
		g->players[i]->playing = true;
		//get name of player
		char name[NAMESIZE];
		printf("What is the name of player %d?\n", (i+1));
		scanf("%s", name);
		strcpy(g->players[i]->name, name);
	}
	g->players[DECK] = malloc(sizeof(Player));
	strcpy(g->players[DECK]->name, "DECK");
}

/** Initialie a full deck of cards for the game to use.
 */
void createDeck(Game *g)
{
	g->circles = false;
	g->highest.val = 0;
	g->highest.suit = SENTINEL;
	g->suit = SENTINEL;
	addSent(g->players[DECK], 0);
	int n,j;
	for(n = 1; n <= 15; n++)
	{
		for(j = 1; j < 5; j++)
		{
			Card *tmp;
			tmp = malloc(sizeof(Card));
			tmp->val = n;
			tmp->suit = j;
			CNode *n;
			n = malloc(sizeof(CNode));
			n->card = tmp;
			n->next = g->players[DECK]->hand;
			g->players[DECK]->hand = n;
		}
	}
	addSent(g->players[DECK], 16);
}

/*shuffle deck and deal to players*/
void dealRound(Game *g, int dealer)
{
	srand(time(NULL));
	//shuffle, Fisher-Yates shuffle algorithm
	for(int i = SIZEOFDECK; i > 0; i--)
	{
		int k = (rand()%i)+1;
		swap(cardAtIndex(g->players[DECK]->hand, k),
		 cardAtIndex(g->players[DECK]->hand, i));
	}
	//set up sentinel node with 0 card value
	for(int i = 0; i < 4; i++)
	{
		addSent(g->players[i], 0);
		//link last node back to itself
		g->players[i]->hand->next = g->players[i]->hand;
	}
	//deal all the real cards
	for(int i = (1 + dealer); i <= (60 + dealer); i++)
	{
		addCard(g->players[i%4],
	 cardAtIndex(g->players[DECK]->hand, i-dealer));
	}
	//set up sentinel node with 16 card value
	for(int i = 0; i < 4; i++)
		addSent(g->players[i], 16);
	//sort the cards
	for(int p = 0; p < 4; p++)
	{
		for(int i = 1; i <= 15; i++)
		{
			for(int k = 1; k < 15; k++)
			{
		    Card c =*(cardAtIndex(g->players[p]->hand, k)->card);
		    Card c2=*(cardAtIndex(g->players[p]->hand,k+1)->card);
				if(c.suit > c2.suit || (c.suit == c2.suit
				 && c.val > c2.val))
				{
				swap(cardAtIndex(g->players[p]->hand, 
				k),cardAtIndex(g->players[p]->hand, k+1));
				}
			}
		}
	}
}

/** Print the names and points of all the players.
 */
void printPoints(Game *g)
{
	printf("\nCURRENT SCORE\n");
	for(int i = 0; i < 4; i++)
	{
		printf("%s has %d points\n", g->players[i]->name, g->players[i]->points);
	}
}

/*main logic to see if a card is winning*/
void processplay(Game *g, Player *p, Card c)
{
	if(c.suit == CIRCLE)
	{
		g->circles = true;
	}
	if(!g->highest.suit)
	{
		g->highest = c;
		*(g->winning) = p;
		g->suit = c.suit;
	}
	else if(c.suit == g->highest.suit)
	{
		if(c.val > g->highest.val)
		{
			g->highest = c;
			*(g->winning) = p;
		}
	}
	else if(c.suit == CIRCLE)
	{
		g->highest = c;
		*(g->winning) = p;
	}
}

/*see if the card played is valid*/
bool validPlay(Game *g, Player *p, int ind)
{
	if (!g->highest.suit)
	{
		if(g->circles || cardAtIndex(p->hand, ind)->card->suit < 4)
		{
			return true;
		}
		else
		{
			CNode **curr;
			curr = malloc(sizeof(void*));
			*curr = p->hand;
			while((*curr)->card->val)
				if(((*curr) = (*curr)->next)->card->suit < 4)
				{
					free(curr);
					return false;
				}
			free(curr);
		}
	}
	if(cardAtIndex(p->hand, ind)->card->suit != CIRCLE 
		&& g->suit != (cardAtIndex(p->hand, ind)->card->suit))	
	{
		CNode **curr;
		curr = malloc(sizeof(void*));
		*curr = p->hand;
		while((*curr)->card->val)
		{
			if((*curr)->card->suit == g->suit)
			{	
				free(curr);
				return false;
			}
			*curr = (*curr)->next;
		}
		free(curr);
	}
	return true;
}

/** Play only one trick of the game.
 */
void playTrick(Game *g)
{
	int i = -1;
	while(!g->players[++i]->lead);
	g->players[i]->lead = false;
	int k = i;
	char ch[2];
	do{
		if (g->players[k%4]->playing)
		{
			printf("\n\n****CURRENT HAND OF %s****\n\n", 
			g->players[k%4]->name);
			printHand(g->players[k%4]);
			printf("\nThe winning card is: %d of %s\n", 
			g->highest.val, SuitNames[g->highest.suit]);
			printf("\nPlay which card?\n");
			scanf("%1s", ch);
			int ct = numOfCards(g->players[k%4]);
			while((ch[0]-'a'+1) < 1 || (ch[0]-'a') > ct)
			{
				printf("Please enter a valid letter\n");
				scanf("%1s", ch);
			}
			while(!validPlay(g, g->players[k%4],ch[0] - 'a' + 1))
			{
				printf("\nPlease play a card of the suit "
		"if you have it, or a circle only if circle"
		"are broken (or you are the one breaking them)\n");
				printf("\nPlay which card?\n");
				scanf("%1s", ch);
			}
			processplay(g, g->players[k%4], 
				play(g->players[k%4],ch[0]));
			printf("\n\n//NEW HAND//\n\n");
			printHand(g->players[(k++ % 4)]);
		}
		else
		{
			k++;
		}
	}while(k%4 != i);
	printf("\nTHE WINNER OF THAT TRICK WAS: %s\n",(*(g->winning))->name);
	(*(g->winning))->points++;
	(*(g->winning))->lead = true;
	g->highest.val = 0;
	g->highest.suit = SENTINEL;
	printPoints(g);
	
}
void save(Game *g)
{
	FILE *sFile;
	sFile = fopen("save.dat", "w+b");
	for(int i = 0; i < 5; i++)
	{
		int ct = numOfCards(g->players[i]);
		g->players[i]->count = ct;
		fwrite(g->players[i], sizeof(Player), 1, sFile);
		for(int k = 0; k < ct+1; k++)
		{
			Card *c;
			c = malloc(sizeof(Card));
			*c = play(g->players[i], 'a');
			fwrite(c, sizeof(Card), 1, sFile);
			free(c);
		}
	}
	fclose(sFile);
}

void saveTest(Game *g)
{
	for(int i = 0; i < 5; i++)
	{
		FILE *sFile;
		sFile = fopen("save.dat", "w+b");
		int ct = numOfCards(g->players[i]);
		g->players[i]->count = ct;
		for(int i = 0; i < 5; i++)
			printHand(g->players[i]);
		fwrite(g->players[i], sizeof(Player), 1, sFile);
		for(int k = 0; k < ct+1; k++)
		{
			Card *c;
			c = malloc(sizeof(Card));
			*c = play(g->players[i], 'a');
			fwrite(c, sizeof(Card), 1, sFile);
			free(c);
		}
		killList(&(g->players[i]->hand));
		free(g->players[i]);
		g->players[i] = malloc(sizeof(Player));
		rewind(sFile);

		fread(g->players[i], sizeof(Player), 1, sFile);
		g->players[i]->hand = NULL;
		g->players[i]->hand = malloc(sizeof(CNode));
		addSent(g->players[i], 0);
		for(int k = 0; k <g->players[i]->count; k++)
		{
			Card *c;
			c = malloc(sizeof(Card));
			fread(c, sizeof(Card), 1, sFile);
			CNode *tmp;
			tmp = malloc(sizeof(CNode));
			tmp->card = c;
			addCard(g->players[i], tmp);
		}
		addSent(g->players[i], 16);  
		fclose(sFile);
	}
	
}
/*load from a binary data file*/
void load(Game *g)
{
	FILE *lFile;
	lFile = fopen("save.dat", "rb");
	if(lFile == NULL)
	{
		printf("There wasn't an input file. Exiting program.\n");
		assert(1 == 0); // quick and dirty GTFO
	}		
	g->players = malloc(5 * sizeof(void*));
	for(int i = 0; i < 5; i++)
	{
		g->players[i] = malloc(sizeof(Player));
		fread(g->players[i], sizeof(Player), 1, lFile);
		g->players[i]->hand = NULL;
		addSent(g->players[i], 0);
		for(int k = 0; k <g->players[i]->count+1; k++)
		{
			Card *c;
			c = malloc(sizeof(Card));
			fread(c, sizeof(Card), 1, lFile);
			CNode *tmp;
			tmp = malloc(sizeof(CNode));
			tmp->card = c;
			addCard(g->players[i], tmp);
			free(tmp->card);
			free(tmp);
		}
		addSent(g->players[i], 16);  
	}
	fclose(lFile);
}

/** Play a round (hand) of the game, from dealing through to printing points.
 */
bool playRound(Game *g)
{
	for(int i = 0; i < 15; i++)
	{
		playTrick(g);
		printf("\nPlay another trick?(y/n)\n");
		char ch[] = " \0";
		while(ch[0] != 'n' && ch[0] != 'y' && i != 15) 
		{
			scanf("%1s", ch);
			ch[0] = tolower(ch[0]);
		}
		if(ch[0] == 'n')
			return 0;
	}
	printf("\nPlay another round?(y/n)\n");
	char ch[] = " \0";
	while(ch[0] != 'n' && ch[0] != 'y') 
	{
		scanf("%1s", ch);
		ch[0] = tolower(ch[0]);
	}
	
	return (ch[0] == 'y');
} 
/*free all the memory*/
void killGame(Game *g)
{
	for(int i = 0; i < 5; i++)
	{
		free(g->players[i]);
	}
	free(g->players);
	free(g->winning);
	free(g);
	
}
