/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#ifndef GAME_H
#define GAME_H

#include "player.h"

typedef struct {
	Player **players;
	Card highest;
	Player **winning;
	bool circles;
	suit_v suit;
} Game;

void initPlayers(Game *g);

void createDeck(Game *g);

void dealRound(Game *g, int dealer);

void processplay(Game *g, Player *p, Card c);

bool validPlay(Game *g, Player *p, int ind);

void playTrick(Game *g);

void save(Game *g);

void saveTest(Game *g);

void load(Game *g);

bool playRound(Game *g);

void killGame(Game *g);

#endif
