/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#include "game.h"

int main()
{
	Game *g;
	g = malloc(sizeof(Game));
	g->winning = malloc(sizeof(void*));
	g->highest.suit = 0;
	g->highest.val = 0;
	int dealer = 0;
	printf("Would you like to load a previous game?\n");
	char ch[] = " \0";
	while(ch[0] != 'n' && ch[0] != 'y') 
	{
		scanf("%1s", ch);
		ch[0] = tolower(ch[0]);
	}
	if(ch[0] == 'y')
	{
		load(g);
	}
	else
	{
		initPlayers(g);
		g->players[(dealer + 1)%4]->lead = true;	
		createDeck(g);
		dealRound(g, dealer);
		dealer = dealer + 1 % 4;
	}
	while(playRound(g))
	{
		for(int i = 0; i < 5; i++)
			killList(&(g->players[i]->hand));
		g->players[(dealer+1)%4]->lead = true;	
		createDeck(g);
		dealRound(g, dealer);
		for(int i = 0; i < 4; i++)
		{	
			printf("Is %s playing this round? (y/n)\n",
			 g->players[i]->name);
			char ch[] = " \0";
			while(ch[0] != 'n' && ch[0] != 'y') 
			{
				scanf("%1s", ch);
				ch[0] = tolower(ch[0]);
			}
			g->players[i]->playing = ch[0] == 'y';
		}
	dealer = (dealer+1)%4;
	}
	strcpy(ch, " \0");	
	printf("Would you like to save the game before it closes?\n");
	while(ch[0] != 'n' && ch[0] != 'y') 
	{
		scanf("%1s", ch);
		ch[0] = tolower(ch[0]);
	}
	if(ch[0] == 'y')
		save(g);
	for(int i = 0; i < 5; i++)
		killList(&(g->players[i]->hand));
	killGame(g);
	return 1;
}
