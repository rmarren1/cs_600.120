/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#include "player.h"
#include "card.h"

const char* suitNames[] = {"SENTINEL\0", "(Sq) ■ \0", "(Tr) ▲\0", "(Di) ◆\0", "(Ci) ●\0"};

void printHand(const Player *p)

{	
	CNode *curr = p->hand->next;
	char i = 'a';
	printf("%s\n", p->name);
	while(curr->card->val)
	{
		printf("%c) %s %d\n", i++, suitNames[curr->card->suit], curr->card->val);
		curr = curr->next;	
	}
}

void addCard(Player *p, CNode *c)
{
	CNode *n;
	n = malloc(sizeof(CNode));
	n->card = malloc(sizeof(Card));
	*(n->card) = *(c->card);
	n->next = p->hand;
	p->hand = n;
}

void addSent(Player *p, int valu)
{
	Card *flag;
	flag = malloc(sizeof(Card));
	flag->val = valu;
	flag->suit = SENTINEL;
	CNode *sent;
	sent = malloc(sizeof(CNode));
	sent->card = flag;
	sent->next = p->hand;
	p->hand = sent;
	
}

Card play(Player *p, char letter)
{
	CNode **tmp;
	tmp = malloc(sizeof(void*));
	*tmp = cardAtIndex(p->hand, letter - 'a' + 1);
	cardAtIndex(p->hand, letter - 'a')->next = cardAtIndex(p->hand, letter - 'a' + 2);
	Card c = *((*tmp)->card);
	free((*tmp)->card);
	free(*tmp);
	free(tmp);
	return c;	
}

int numOfCards(Player *p)
{
	int ct = 0;
	CNode **tmp;
	tmp = malloc(sizeof(void*));
	*tmp = p->hand->next;
	while((*tmp)->next->card->val)
	{
		ct++;
		*tmp = (*tmp)->next;
	}
	free(tmp);
	return ct;
}
