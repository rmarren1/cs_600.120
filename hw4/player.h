/*Ryan Marren
 *600.120
 *10/16/14
 *Assignment 4 
 *845-548-4791
 *rmarren1
 *rmarren1@jhu.edu
*/
#ifndef PLAYER_H
#define PLAYER_H
#include "card.h"
#define NAMESIZE 20

typedef struct {
	CNode *hand;
	char name[NAMESIZE];
	bool lead;
	int points;
	bool playing;
	int count;
} Player;

void printHand(const Player *p);

void addCard(Player *p, CNode *c);

void addSent(Player *p, int valu);

Card play(Player *p, char letter);

int numOfCards(Player *p);

#endif
