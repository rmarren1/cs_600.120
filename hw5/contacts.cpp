#include "contacts.h"
#include "faves.h"

bool Contacts::command()
{
	vector<string> args;
	string input;
	string swag = string(21, '*');
	cout << endl << swag  << endl << "PLEASE ENTER A COMMAND" << endl;
	cout << "Try 'help' for instructions." << endl
	<< "Try 'quit' to exit." << endl;
	cout <<  swag << endl;
	getline(cin, input);
	stringstream ss(input);
	string arg;
	bool f = false;
	while(getline(ss, arg, ' '))
	{
		args.push_back(arg);
	}
	for(int i = 0; i < 4; i++)
	{
		args.push_back("");
	}
	args[1] == "-f" ? f = true : f = false;
	if(args[0] == "add" || args[0] == "edit")
	{
		f ? (contacts.end() != contacts.find(args[2]) ?
		fave.Faves::add(args[2]) :  args[2] == "" ? 
		say("Please put an argument! -- e.g. add -f Bill") :  
		say("No contact " + args[2] + " to favorite!")) 
		: add(args[1]);
	}
	else if(args[0] == "delete")
	{
		int result = -1;
		stringstream conv(args[2]);
		if( !(conv >> result) )
			result = -1;
		f ? fave.Faves::del(result) : del(args[1]);
	}
	else if(args[0] == "print")
	{
		f ? fave.Faves::print() : print(args[1]);
	}
	else if (args[0] == "help")
	{
		help();
	}
	else if (args[0] == "quit")
	{
		return false;
	}
	else if (args[0] == "move")
	{
		unsigned t, p;
		t = p = 0;
		stringstream conv(args[2]);
		if( !(conv >> t) )
			t = 0;
		stringstream conv2(args[3]);
		if( !(conv2 >> p) )
			p = 0;	
		f ? fave.Faves::move(t, p) : say("Still put -f for move");
	}
	else
	{
		say("Command \"" + args[0] + "\" is invalid!");
	}
	return true;
}	

void Contacts::help()
{
	string bars = string(45, '/');
	cout << endl << bars << endl;
	cout << "WELCOME TO THE CONTACT MANAGER 2.0" << endl
	<< "Commands are: add, edit, delete, print, move and quit" 
	<< endl << endl
	<< "To add to, edit, delete from, or print a certain contact" << endl
	<< " just put the contact's name after the command!" << endl << endl
	<< "To use the favorites list, add a '-f' option between" << endl
	<< " the command and the argument!" << endl << endl
	<< "Examples:" << endl << "add -- create a new contact" << endl
	<< "add Bill -- add a number to Bill's contact." << endl
	<< "edit -- edit a number or add one if it doesnt exist" << endl
	<< "edit Bill -- edit a number in Bills contact, or add one" << endl
	<< "delete -- delete a contact or a number from it" << endl
	<< "delete Bill -- delete Bill's contact or a number in it." << endl
	<< "print -- print all the contacts" << endl
	<< "print Bill -- print just Bill's contact." << endl
	<< "add -f Bill -- add bill to the favorites list (arg required)." 
	<< endl << "print -f -- print the favorites list" << endl
	<< "delete -f 3 -- delete the 3rd favorite (arg not required)."<<endl
	<< "move -f 4 5 -- swap favorites 4 and 5 (args required)." << endl;
	cout << bars << endl;
}
void Contacts::say(string message)
{
	string frame = string(message.length(), '/');
	cout << endl << frame << endl << message << endl << frame << endl;

}
void Contacts::add(string input)
{
	string number;
	string name;
	if(input != "")
	{
		name = input;
		if(contacts.find(name) == contacts.end())
		{
			say("There is no contact " +name+ " to change!");
			return;
		}
	}
	else
	{
		cout<<endl<<"Enter the name of the contact"<<endl;
		cin >> name;
		cin.ignore(1, '\n');
	}
	string type;
	cout<< endl<< endl << "What kind of number?" << endl;
	pair<string, string> pair;
	getline(cin, type);
	pair.first = type;	
	cout << endl << "Enter the number" << endl;
	cin >> pair.second;
	cin.ignore(1, '\n');	
	contacts[name][pair.first] = pair.second;
	say("Added \"" + pair.first + 
		" - " + pair.second + "\" to "
	+ name + "\'s number list.");
}

void Contacts::del(string input)
{
	string name;
	if(input == "")
	{
		cout << endl << "Delete from which contact?" << endl;
		cin >> name;
		cin.ignore(1, '\n');
	}
	else
	{
		name = input;
	}
	map<string, map<string, string> >::iterator it = 
	contacts.find(name);
	if(it == contacts.end())
	{
		say(name +  " is not a contact.");
	} 
	else
	{
		char ans;
		cout << endl << "Delete entire contact (c)"
		" or just a number (n) ?" << endl;
		cin >> ans;
		cin.ignore(1, '\n');
		while(ans != 'c' && ans != 'n')
		{
			cout << "Enter a c or an n" << endl;
			cin >> ans;
			cin.ignore(1, '\n');
		}
		if (ans == 'c')
		{
			contacts.erase(it);
			say(name + "'s contact has been deleted.");
		}
		else
		{
			string type;
			print(it);
			cout << endl <<"Delete which type?"<<endl;
			getline(cin, type);
			string msg = it->second.erase(type) ? 
	"Deleted \"" + type + "\" from " + name + "'s contact."
	:"Failure, no "+type+" # to erase!";
			say(msg);
		}
	}
}
	
void Contacts::print()
{
	for( map<string, map<string, string> >::const_iterator it 
	= contacts.begin(); it != contacts.end(); it++)
	{
		print(it);	
	}

}

void Contacts::print(map<string, map<string, string> >::const_iterator it)
{
	string slash = string(10 + it->first.length(), '/');
	cout << endl << slash << endl << "Name:\t" << it->first << endl;
	string line = string(10 + it->first.length(), '_');
	cout << line << endl ;
	for( map<string, string>::const_iterator nu =
	it->second.begin(); nu != it->second.end(); nu++)
	{
		cout << nu->first + "\t" <<
		nu->second << endl;
	}
	cout << slash << endl;
}

void Contacts::print(string name)
{
	if(name == "")
	{
		bool flg = false;
		for( map<string, map<string, string> >::const_iterator it 
		= contacts.begin(); it != contacts.end(); it++)
		{
			flg = true;
			print(it);
		}
		if(!flg){say("No contacts to print.");}
	}
	else
	{
		map<string, map<string, string> >::const_iterator it 
		= contacts.find(name);
		if(it != contacts.end())
		{
			print(it);
		}
		else
		{
			say(name + " is not a contact.");
		}
	}
}
