#ifndef GUARD_CONTACTS_H
#define GUARD_CONTACTS_H
#include <string>
#include <vector>
#include <iostream>
#include <map>
#include <sstream>
#include "faves.h"
using std::string;
using std::vector;
using std::map;
using std::stringstream;
using std::cout;
using std::cin;
using std::endl;
using std::pair;

class Contacts
{
	map<string, map<string, string> > contacts;
	Faves fave;
public:
	bool command();
	void help();
	void add(string);
	void del(string name = "");
	void print();
	void print(string name = "");	
	void print(map<string, map<string, string> >::const_iterator it);
	void say(string message);
};
#endif
