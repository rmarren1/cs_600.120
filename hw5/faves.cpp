#include "contacts.h"
#include "faves.h"

void Faves::add(string ct)
{
	unsigned pos = 0;
	if(pos == 0)
	{
		Faves::print();
		cout<<endl << "Insert " + ct +" at which position?" << endl;
		cin >> pos;
		cin.ignore(1, '\n');
	}
	if( pos > faves.size())
	{
		faves.push_back(ct);
	}
	else
	{
		vector<string>::iterator it = faves.begin() + pos;
		faves.insert(it, ct);
	}
	Faves::print();
}
void Faves::print()
{
	vector<string>::size_type sz= faves.size();
	cout << endl << "FAVORITES" << endl << string(30, '/') << endl;
	for(unsigned i = 0; i < sz; i++)
		cout << i << ". " << faves[i] << endl;
	cout << string(30, '/') << endl;
}

void Faves::del(int pos)
{
	if(pos == -1)
	{
		print();
		cout << endl << "Delete which favorite position?" << endl;
		cin >> pos;
		cin.ignore(1, '\n');
	}
	unsigned pos2 = pos;
	if( pos2 > faves.size()-1)
	{
		cout << endl << string(30, '/') << endl <<
		"No favoritre at position " << pos << "." << endl <<
		string(30, '/') << endl;
		return;
		
	}
	faves.erase(faves.begin() + pos);
	print();
}

void Faves::move(unsigned target, unsigned position)
{
	if(faves.size() < 2){return;}
	vector<string>::size_type sz_t = target > faves.size() ? 
	faves.size()-1 : target;
	vector<string>::size_type sz_p = position > faves.size() ?
	faves.size()-1 : position;
	string tmp = faves[sz_t];
	faves[sz_t] = faves[sz_p];
	faves[sz_p] = tmp;
	print();
}

