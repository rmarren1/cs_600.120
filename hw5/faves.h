#ifndef GUARD_FAVES_H
#define GUARD_FAVES_H
#include <vector>
#include <string>
using std::vector;
using std::string;
class Faves
{
	vector<string> faves;
public:
	void add(string);
	void print();
	void move(unsigned, unsigned);
	void del(int);
};
#endif
