#include "Card.h"

Card::Card(int shape, int num)
{
	setSuit(shape);
	setFace(num);
}

Card::Card(const Card & other)
{
	suit = other.suit;
	face = other.face;
}

void Card::setSuit(int shape)
{
	if(shape >= 1 && shape <= 4)
	{
		suit = shape;
	}
	else
	{
		suit = 0;
	}
}

void Card::setFace(int num)
{
	if(num >= 1 && num <= 60)
	{
		face = num;
	}
	else
	{
		face = 0;
	}
}

int Card::getSuit() const
{
	return suit;
}

int Card::getFace() const
{
	return face;
}

int Card::rank() const
{
	return face * suit;
}

bool Card:: operator< (const Card & other) const
{
	return this->rank() < other.rank();
}
bool Card:: operator<= (const Card & other) const
{
	return this->rank() <= other.rank();
}
bool Card:: operator== (const Card & other) const
{
	return this->rank() == other.rank();
}
