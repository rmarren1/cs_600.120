#ifndef GUARD_CARD
#define GUARD_CARD
class Card
{
private:
	int suit;
	int face;
public:
	enum suits {Triangle, Square, Diamond, Circle};
	Card(int, int);	
	Card(const Card &);

	void setSuit(int num);
	void setFace(int shape);
	int getSuit() const;
	int getFace() const;
	int rank() const;
	bool operator< (const Card & ) const;
	bool operator<=(const Card & ) const;
	bool operator==(const Card & ) const;
};
#endif
