#include "Card.h"
#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <random>

using std::cout;
using std::string;
using std::endl;
using std::vector;

void message(string msg)
{
	string bars(50, '/');
	string spacing((50 - msg.length()) / 2 - 1, ' ');
	cout << bars << endl << '/' << spacing << msg << spacing << '/'
	<< endl << bars << endl << endl;
}
int main()
{
	message("Thoroughly testing 100,000 construction inputs"); 
	std::uniform_int_distribution<int> rsuit(0, 4);
	std::uniform_int_distribution<int> rval(0, 60);
	std::default_random_engine generator;
	for(int suit = -1000; suit < 1000; suit++)
		for(int face = -1000; face < 1000; face++)
		{
			Card tmp(suit, face);
			Card tmpcpy(tmp);
			face >= 1 && face <= 60 ?
				assert(tmp.getFace() == face) :
				assert(tmp.getFace() == 0);
			suit >= 1 && suit <= 4 ?
				assert(tmp.getSuit() == suit) :
				assert(tmp.getSuit() == 0);
			assert(tmp.getFace() == tmpcpy.getFace());
			assert(tmp.getSuit() == tmpcpy.getSuit());
			assert(tmp.rank() == tmpcpy.rank());
			assert(tmp.rank() == tmp.getFace() * tmp.getSuit());
			int rand_suit = rsuit(generator);
			int rand_face = rval(generator);
			Card rand(rand_suit, rand_face);
			tmp.rank() < rand.rank() ?
				assert(tmp < rand) :
				assert(!(tmp < rand));
			
		}

	message("Setters and Getters work correctly");
	message("Constructor works for all valid input");
	message("Copy Constructor works correctly");
	message("Constructor works for invalid input");
	message("Rank function properly ranks cards");
	message("< operator works for 400,000 random comparisons");
	
}

