#ifndef GUARD_OLIST_H
#define GUARD_OLIST_H
#include <cstddef>
#include <iostream>

template <typename T>
struct node
{
	T *val;
	node<T> *next;
};

template <class T>
class OList
{
	
	node<T> *head;
public:	
	OList();
	OList(OList & other);
	~OList();
	void insert(const T);
	int size() const;
	T * get(const int) const;
	void clear();
	int count(const T) const;
	void remove(const T);
	void uniquify();
};

template <class T>
OList<T>::OList()
{
	head = new node<T>;
	*head = {NULL, NULL};
}

template <class T>
OList<T>::OList(OList & other)
{
	head = new node<T>;
	node<T> *curr = other.head;
	*head = {NULL, NULL};
	while(curr->next)
	{
		curr = curr->next;
		insert(*(curr->val));
		
	}
}

template <class T>
OList<T>::~OList()
{
	clear();
	delete head;
	head = NULL;
}

template <class T>
void OList<T>::insert(const T valu)
{
	node<T> *curr = head;
	while(curr->next && *(curr->next->val) < valu)
		curr = curr->next;
	node<T> *n = new node<T>;
	T *i = new T(valu);
	*n = {i, curr->next};
	curr->next = n;
}

template <class T>
int OList<T>::size() const
{
	int i = 0;
	node<T> *curr = head;
	while(curr && curr->next)
	{
		curr = curr->next;
		i++;
	}
	return i;
}

template <class T>
void OList<T>::clear()
{
	node<T> *curr = head;
	while(curr)
	{
		head = head->next;
		delete curr->val;
		curr->val = NULL;
		delete curr;
		curr = NULL;
		curr = head;
	}
	head = new node<T>;
	*head = {NULL, NULL};
}

template <class T>
T * OList<T>::get(const int i) const
{	
	if(i <= size() && i > 0)
	{
		node<T> * curr = head;
		for(int j = 0; j < i; j++)
			curr = curr->next;
		return curr->val;
	}
	return NULL;
}


template <class T>
int OList<T>::count(const T t) const
{
	int i = 0;;
	node<T> * curr = head;
	while(curr && curr->next)
	{
		curr = curr->next;
		if(*(curr->val) == t)
			i++;		
	}
	return i;
}

template <class T>
void OList<T>::remove(const T i)
{
	node<T> * curr = head;
	while(curr && curr->next)
	{
		if(*(curr->next->val) == i)
		{
			node<T> ** tmp = new node<T>*;
			*tmp = curr->next;
			curr->next = curr->next->next;
			delete (*tmp)->val;
			(*tmp)->val = NULL;
			delete *tmp;
			*tmp = NULL;
			delete tmp;
			tmp = NULL;
			
		}
		curr = curr->next;
	}
}

template <class T>
void OList<T>::uniquify()
{
	node<T> * curr = head;
	while(curr && curr->next)
	{
		curr = curr->next;
		if(count(*(curr->val)) > 1)
			for(int i = 0; i < count(*(curr->val)) - 1; i++)
			{
				remove(*(curr->val));
				uniquify();
				return;
			}
	}
}
#endif
