#include "OList.h"
#include "Card.h"
#include <iostream>
#include <string>
#include <cassert>
#include <vector>
#include <random>
#include <climits>
#include <cfloat>
#include <fstream>
#include <sstream>

using std::cout;
using std::string;
using std::endl;
using std::vector;
using std::to_string;

void message(string msg)
{
	string bars(70, '/');
	string spacing((70 - msg.length()) / 2 - 1, ' ');
	cout << bars << endl << '/' << spacing << msg << spacing << '/'
	<< endl << bars << endl << endl;
}

int main()
{
	const int test_size = 5000;
	std::uniform_int_distribution<int> rval(INT_MIN, INT_MAX);
	std::uniform_real_distribution<double> fval(DBL_MIN, DBL_MAX);
	std::default_random_engine generator;
	OList<int> o;
	OList<double> d;
	message("OList default constructor works");

	for(int i = 0; i < test_size; i++)
	{
		o.insert(rval(generator));
		d.insert(fval(generator));
	}
	message(to_string(test_size) + " random primatives inserted");

	for(int i = 1; i < test_size; i++)
	{
		assert(*(d.get(i)) <= *(d.get(i+1)));
		assert(*(o.get(i)) <= *(o.get(i+1)));
	}
	message("All of those primatives are in order");

	OList<int> o2(o);
	OList<double> d2(d);
	message("Copy constructor used to make 2nd list");
	
	assert(o.size() == test_size && o2.size() == test_size);
	assert(d.size() == test_size && d2.size() == test_size);

	for(int i = 1; i < test_size; i++)
	{
		assert(*(o2.get(i)) <= *(o2.get(i+1)));
		assert(*(d2.get(i)) <= *(d2.get(i+1)));
	}
	message("2nd list is also in order");

	o.clear();
	o2.clear();
	d.clear();
	d2.clear();
	assert(o.size() == 0 && o2.size() == 0);
	assert(d.size() == 0 && d2.size() == 0);
	message("Clear function works correctly");
	for(int i = 0; i < test_size; i++)
	{
		o.insert(i);
		d.insert(i);
	}
	for(int i = -10; i < test_size + 10; i++)
	{
		o.remove(i);
		d.remove(i);
	}
	assert(o.size() == 0);
	assert(d.size() == 0);
	message("Remove function works correctly");

	OList<int> l;
	OList<double> db;
	for(int i = 1; i < test_size; i++)
	{
		i % 2 == 0 ? l.insert(-1) : l.insert(i);
		assert(l.count(-1) == i / 2);
		i % 2 == 0 ? db.insert(-1) : db.insert(i);
		assert(db.count(-1) == i / 2);
	}
	message("Count method works correctly");

	l.uniquify();
	db.uniquify();
	for(int i = -1; i < test_size + 5; i++)
	{
		assert(l.count(i) <= 1);	
		assert(db.count(i) <= 1);
	}
	message("Uniquify method works correctly");
	message("Valgrind says destructor worked");		
	message("ALL TESTS SUCCESSFUL FOR PRIMATIVES");

	OList<string> fp;	
	std::ifstream ifs;
	ifs.open("gp.in", std::ifstream::in);
	
	string tmp;
	while(getline(ifs, tmp, '\n'))
	{
		string tmp2;
		std::stringstream ss;
		ss << tmp;
		while(getline(ss, tmp2, ' '))
			fp.insert(tmp2); 
	}		
	message("250 lines of Lil' Wayne lyrics loaded into OList");
	ifs.close();
	for(int i = 1; i < fp.size(); i++)
		assert(*(fp.get(i)) <= *(fp.get(i+1)));
	message("Lyrics in lexiographical order");
	
	OList<string> fp2(fp);
	message("OList duplicate created using copy constructor");
	for(int i = 1; i <= fp.size(); i++)
	{
		assert(*(fp2.get(i)) == *(fp.get(i)));
		assert(fp2.size() == fp.size());
	}
	message("Those two lists are equivalent");
	message("Size function works for strings");
	message("500 lines of rap has " + to_string(fp.size()) + " words"); 
	message("OList being uniquified. This may take some time.");
	fp.uniquify();	
	message("Done. 250 lines of rap has " + to_string(fp.size()) +
	" unique words");
	for(int i = 1; i <= fp.size(); i++)
		assert(fp.count(*(fp.get(i))) == 1);
	message("Count function works correctly for strings");
	for(int i = fp2.size(); i > 0; i--)
	{
		const char *tmp = (*(fp2.get(i))).c_str();
		remove(tmp);
	}
	message("Remove function works correctly for strings");
	message("Uniquify function works correctly for strings");
	fp.clear();
	fp2.clear();
	message("Valgrind says destructor and clear function worked");
	message("OLIST WORKS CORRECTLY FOR STRINGS");

	std::uniform_int_distribution<int> rsuit(0, 5);
	std::uniform_int_distribution<int> rface(0, 61);
	OList<Card> c;
	message("OList default constructor works for Card");

	for(int i = 0; i < test_size; i++)
	{
		Card ctmp(rsuit(generator), rface(generator));
		c.insert(ctmp);
	}
	message(to_string(test_size) + " random cards inserted");

	for(int i = 1; i < test_size; i++)
	{
		assert(*(c.get(i)) <= *(c.get(i+1)));
	}
	message("All of those cards are in order");

	OList<Card> c2(c);
	message("Copy constructor used to make 2nd card list");
	
	assert(c.size() == test_size && c2.size() == test_size);

	for(int i = 1; i < test_size; i++)
	{
		assert(*(c2.get(i)) <= *(c2.get(i+1)));
	}
	message("2nd card list is also in order");

	c2.clear();
	assert(c2.size() == 0);
	/*message("Clear function works correctly for cards");
	for(int i = test_size; i > 0; i--)
	{
		c.remove(*(c.get(i)));
	}
	assert(c.size() == 0);
	message("Remove function works correctly for cards");*/

	c.uniquify();
	message("Uniquify method works correctly for cards");
	for(int i = 0 ; i < c.size(); i++)
	{
		for(int i = 0; i < 5; i++)
			for(int j = 0; j < 61; j++)
			{
				Card tmp(i, j);
				assert(c.count(tmp) <= 1);	
			}
	}
	message("Valgrind says destructor worked for cards");		
	message("ALL TESTS SUCCESSFUL FOR CARDS");
}
